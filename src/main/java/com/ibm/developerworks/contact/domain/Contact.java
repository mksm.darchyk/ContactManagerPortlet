package com.ibm.developerworks.contact.domain;

public class Contact {
	private String contactId;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	
	public Contact(String contactId,String firstName, String lastName, String email, String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	public Contact(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getContactId() {
		return contactId;
	}
	public void setContactId(String contactId) {
		this.contactId = contactId;
	}
	public String toString() {
		StringBuffer contactStr = new StringBuffer("Contact =[ First Name = ");
		contactStr.append(firstName);
		contactStr.append(", Contact Id = ");
		contactStr.append(contactId);
		contactStr.append(", Last Name = ");
		contactStr.append(lastName);
		contactStr.append(",  Email = ");
		contactStr.append(email);
		contactStr.append(",  Phone Number = ");
		contactStr.append(phoneNumber);
		contactStr.append(" ]");
		return contactStr.toString();
	}
	
}
