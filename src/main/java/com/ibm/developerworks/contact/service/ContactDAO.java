package com.ibm.developerworks.contact.service;

import java.util.ArrayList;

import com.ibm.developerworks.contact.domain.Contact;

public interface ContactDAO {
	public ArrayList getContactList();
	public Contact getContact(String contactId);
	public int insertContact(Contact contact);
	public int updateContact(Contact contact);
	public int deleteContact(String contactId);
}
