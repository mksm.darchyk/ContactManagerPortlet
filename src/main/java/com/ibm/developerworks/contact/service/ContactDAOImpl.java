package com.ibm.developerworks.contact.service;

import java.util.ArrayList;

import com.ibm.developerworks.contact.domain.Contact;


public class ContactDAOImpl implements ContactDAO{

	HashmapContact hashMapContact = new HashmapContact();

	public void setHashMapContact(HashmapContact hashMapContact) {
		this.hashMapContact = hashMapContact;
	}

	public Contact getContact(String contactId) {
		Contact updateContact = hashMapContact.getContact(contactId);
		return updateContact;
	}

	public ArrayList getContactList() {
		ArrayList contactList = hashMapContact.getContacts();
		return contactList;
	}

	public int insertContact(Contact contact) {
		hashMapContact.saveContact(contact);
		return 0;
	}

	public int updateContact(Contact contact) {
		hashMapContact.saveContact(contact);
		return 0;
	}
	public int deleteContact(String contactId) {
		hashMapContact.deleteContact(contactId);
		return 0;
	}

}
