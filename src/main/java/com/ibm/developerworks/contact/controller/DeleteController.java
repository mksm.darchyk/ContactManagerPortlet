package com.ibm.developerworks.contact.controller;

import javax.portlet.ActionResponse;

import com.ibm.developerworks.contact.service.ContactDAOImpl;

import com.ibm.developerworks.contact.service.ContactDAO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

@org.springframework.stereotype.Controller
@RequestMapping("delete")
public class DeleteController{

	ContactDAO contactDAO = new ContactDAOImpl();

	@ActionMapping
	protected void processFormSubmission(String contactId, ActionResponse actionResponse){
		contactDAO.deleteContact(contactId);
		actionResponse.setRenderParameter("action", "list");
	}
}
