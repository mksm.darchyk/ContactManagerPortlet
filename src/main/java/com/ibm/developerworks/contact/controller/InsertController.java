package com.ibm.developerworks.contact.controller;

import com.ibm.developerworks.contact.service.ContactDAOImpl;

import org.springframework.web.portlet.bind.annotation.ActionMapping;

import com.ibm.developerworks.contact.domain.Contact;
import com.ibm.developerworks.contact.service.ContactDAO;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@org.springframework.stereotype.Controller
@RequestMapping("insert")
public class InsertController {
    private ContactDAO contactDAO = new ContactDAOImpl();

    @ActionMapping
    public void handleRenderRequest(Contact contact, ActionResponse actionResponse) {
        contactDAO.insertContact(contact);
        actionResponse.setRenderParameter("action", "list");
    }
}
