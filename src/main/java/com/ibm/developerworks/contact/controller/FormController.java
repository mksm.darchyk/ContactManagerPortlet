package com.ibm.developerworks.contact.controller;

import com.ibm.developerworks.contact.domain.Contact;
import com.ibm.developerworks.contact.service.ContactDAOImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.ibm.developerworks.contact.service.ContactDAO;

@org.springframework.stereotype.Controller
@RequestMapping("insertForm")
public class FormController {

	@RenderMapping
	public ModelAndView handleRenderRequest(Model model){
		ModelAndView modelAndView = new ModelAndView("insert");
		model.addAttribute("contact", new Contact());
		return modelAndView;
	}
}
