package com.ibm.developerworks.contact.controller;

import com.ibm.developerworks.contact.domain.Contact;
import com.ibm.developerworks.contact.service.ContactDAO;
import com.ibm.developerworks.contact.service.ContactDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Created by DarchykM on 09.09.2016.
 */
@Controller
@RequestMapping("select")
public class MainController {
    private ContactDAO contactDAO = new ContactDAOImpl();

    @RenderMapping
    public ModelAndView handleRenderRequest(Model model){
        ModelAndView modelAndView = new ModelAndView("list");
        model.addAttribute("contactList", contactDAO.getContactList());
        return modelAndView;
    }
}
